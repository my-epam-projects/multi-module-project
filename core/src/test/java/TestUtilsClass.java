import org.example.Utils;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;
class TestUtilsClass {

    @Test
    void TestIsAllPositiveMethod() {
       assertTrue(Utils.isAllPositiveNumbers("26","78"));
       assertFalse(Utils.isAllPositiveNumbers("12","-123"));
    }
}
