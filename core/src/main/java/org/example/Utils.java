package org.example;


import org.gradle.StringUtils;
import org.gradle.exceptions.InvalidInputException;

import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Utils {

    private static final Logger LOGGER = Logger.getLogger(Utils.class.getName());

    public static boolean isAllPositiveNumbers(String... str) {
        return Arrays.stream(str)
                .allMatch(s -> {
                    try {
                        return StringUtils.isPositiveNumber(s);
                    } catch (InvalidInputException e) {
                        LOGGER.log(Level.WARNING, () -> String.format("Invalid input value: %s", s));
                        return false;
                    }
                });
    }


    private Utils() {
    }
}
